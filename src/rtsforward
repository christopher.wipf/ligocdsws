#!/bin/bash

if [ "$#" -lt "1" ]; then
  if [ -z "$RTSFORWARD_SSH_HOST" ]; then
    echo "usage: $0 sshargs"
    exit 1
  elif [ -z "$RTSFORWARD_SSH_ARGS" ]; then
    set -- "$RTSFORWARD_SSH_HOST"
  else
    set -- "$RTSFORWARD_SSH_HOST" "$RTSFORWARD_SSH_ARGS"
  fi
fi

# set pwd
if [[ $BASH_SOURCE = */* ]]; then
  cd -- "${BASH_SOURCE%/*}/" || exit
fi

mkdir -p /tmp/rtsforward/target/gds/param

# obtain site/ifo/ndsserver environment vars using login interactive shell
# bash complains about the forced interactive shell, but it is needed to ensure all vars are defined
sshargs=""
ssh -q $sshargs "$@" -- bash -ilc '"printf \"site=%q\nifo=%q\nSITE=%q\nIFO=%q\nNDSSERVER=%q\nMEDM_EXEC_LIST=%q\n\" \"\$site\" \"\$ifo\" \"\$SITE\" \"\$IFO\" \"\$NDSSERVER\" \"\$MEDM_EXEC_LIST\""' 2> >(grep -v "^bash: no job control\|^bash: cannot set terminal process group" 1>&2) | grep '\(^site\|^ifo\|^SITE\|^IFO\|^NDSSERVER\|^MEDM_EXEC_LIST\)' >/tmp/rtsforward/env
if [ $? -ne 0 ]; then
  echo "error: failure on ssh $@"
  exit 1
fi
echo export site ifo SITE IFO NDSSERVER MEDM_EXEC_LIST >>/tmp/rtsforward/env
source /tmp/rtsforward/env
echo forwarding for site $SITE ifo $IFO

# sshfs should be used first to mount /opt/rtcds
if [ ! -f /opt/rtcds/$site/$ifo/target/gds/param/testpoint.par ]; then
  echo "error: /opt/rtcds for $SITE $IFO is not mounted"
  exit 1
fi

# rewrite testpoint.par for local forwarding
mkdir -p /tmp/rtsforward/target/gds/param
sed -e 's/^hostname=.*$/hostname=localhost/' </opt/rtcds/$site/$ifo/target/gds/param/testpoint.par >/tmp/rtsforward/target/gds/param/testpoint.par

# set LIGO_INJ_MODEL for DTT RPC access
echo '# for DTT RPC access via /tmp/rtsforward/target/gds/param/testpoint.par' >>/tmp/rtsforward/env
echo 'export CDSROOT=/tmp/rtsforward' >>/tmp/rtsforward/env
systems=$(grep '^system=' /tmp/rtsforward/target/gds/param/testpoint.par | sed -e 's/^system=//')
LIGO_INJ_MODEL=""
for sys in $systems; do
  LIGO_INJ_MODEL="$sys,$LIGO_INJ_MODEL"
done
# strip trailing comma
LIGO_INJ_MODEL=${LIGO_INJ_MODEL::-1}
echo export LIGO_INJ_MODEL=$LIGO_INJ_MODEL >>/tmp/rtsforward/env

# (re)start rpcbind for DTT RPC access
echo restarting rpcbind
killall rpcbind
/sbin/rpcbind

# forward NDS ports
# split $NDSSERVER on , and take the first server:port
ndshost=(${NDSSERVER//,/ })
ndshost=${ndshost[0]}
# split server:port on : and take the first one (hostname)
ndshost=(${ndshost//:/ })
ndshost=${ndshost[0]}
forwardargs="-L8087:$ndshost:8087 -L8088:$ndshost:8088"
sed -i -e 's/^NDSSERVER=.*/NDSSERVER=localhost:8088/' /tmp/rtsforward/env
echo using NDS server $ndshost

# forward awgtpman and EPICS ports for each front end host
echo -n "setting up forwarding for front end hosts:"
# uniquify epics connections in case a gateway is used
declare -A epics_hostports_uniq
# do not connect to <disconnected>
epics_hostports_uniq["<disconnected>"]=1
fehosts=$(grep '^hostname=' /opt/rtcds/$site/$ifo/target/gds/param/testpoint.par | sed -e 's/^hostname=//' | sort | uniq)
EPICS_CA_NAME_SERVERS=""
for host in $fehosts; do
  echo -n " $host"
  # collect awgtpman program and port numbers using rpcinfo
  # RPC_PROGNUM_TESTPOINT offset = 0x31002000 = 822091776
  # RPC_PROGNUM_AWG offset = 0x31003000 = 822095872
  progs_hostports=$(ssh -q $sshargs "$@" -- /usr/sbin/rpcinfo -p $host | grep '^\s*82209' | awk "{print \$1, \"$host:\" \$4}")
  # collect EPICS port numbers
  # first collect all the node numbers running on this host
  nodes=$(cat /opt/rtcds/$site/$ifo/target/gds/param/testpoint.par | grep -B 1 -A 1 "^hostname=$host\$" | sed -n 's/^.*node\([0-9]*\)\]$/\1/p')
  # then generate a valid EPICS channel in each node's IOC
  chans=
  for node in $nodes; do
    chans="${IFO}:FEC-${node}_CPU_METER $chans"
  done
  # collect port numbers from cainfo (using interactive login shell to ensure PATH is fully populated)
  epics_hostports=$(ssh -q $sshargs "$@" -- bash -ilc \"cainfo $chans\" 2> >(grep -v "^bash: no job control\|^bash: cannot set terminal process group" 1>&2) | grep '^\s*Host:' | awk '{print $2}')
  for hostport in $epics_hostports; do
    [[ -v "epics_hostports_uniq[$hostport]" ]] && continue
    epics_hostports_uniq[$hostport]=1
    progs_hostports="EPICS $hostport $progs_hostports"
  done
  # convert to array
  progs_hostports=($progs_hostports)
  for (( i=0; i<${#progs_hostports[@]}; i+=2 )); do
    remote_prog=${progs_hostports[i]}
    remote_hostport=${progs_hostports[i+1]}
    # select an available local port at random
    while
      local_port=$(shuf -n 1 -i 49152-65535)
      netstat -atun | grep -q "$local_port" || echo "$local_ports" | grep -q "$local_port"
    do
      continue
    done
    # remember previously selected ports so they are not duplicated
    local_ports="$local_port $local_ports"
    forwardargs="$forwardargs -L$local_port:$remote_hostport"
    if [ "$remote_prog" = "EPICS" ]; then
      EPICS_CA_NAME_SERVERS="127.0.0.1:$local_port $EPICS_CA_NAME_SERVERS"
    else
      ./register_awgtpman_rpc $remote_prog $local_port
    fi
  done
done
echo
# strip trailing space
EPICS_CA_NAME_SERVERS=${EPICS_CA_NAME_SERVERS::-1}
echo '# for EPICS channel access' >>/tmp/rtsforward/env
echo export EPICS_CA_NAME_SERVERS=\"$EPICS_CA_NAME_SERVERS\" >>/tmp/rtsforward/env
echo export EPICS_CA_AUTO_ADDR_LIST=NO >>/tmp/rtsforward/env

sshargs="-o ServerAliveInterval=30 -o ServerAliveCountMax=3 -o ExitOnForwardFailure=yes"

ssh -4fNT $forwardargs $sshargs "$@"
echo To access site $SITE ifo $IFO, use: source /tmp/rtsforward/env

