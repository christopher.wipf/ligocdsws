FROM debian:bookworm-slim
MAINTAINER <christopher.wipf@ligo.org>

# add lscsoft and cdssoft
# multi-arch needed for medm
# install system packages
# update ndscope
RUN apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -qq --no-install-recommends \
  ca-certificates \
  wget \
  && rm -rf /var/lib/apt/lists/* \
  && echo deb [trusted=yes] https://hypatia.aei.mpg.de/lsc-arm64-bookworm ./ >/etc/apt/sources.list.d/lscdebian.list \
  && wget -nv https://apt.ligo-wa.caltech.edu/debian/pool/bookworm/cdssoft-release-bookworm/cdssoft-release-bookworm.deb \
  && dpkg -i cdssoft-release-bookworm.deb \
  && rm cdssoft-release-bookworm.deb \
  && echo 'Package: *' >/etc/apt/preferences \
  && echo 'Pin: origin apt.ligo-wa.caltech.edu' >>/etc/apt/preferences \
  && echo 'Pin-Priority: 100' >>/etc/apt/preferences \
  && dpkg --add-architecture amd64 \
  && apt-get update -qq && DEBIAN_FRONTEND=noninteractive apt-get install -qq --no-install-recommends \
  binfmt-support \
  cds-crtools \
  g++ \
  ghostscript \
  git \
  krb5-user \
  less \
  libsasl2-modules-gssapi-mit \
  locales-all \
  lxde \
  nds2-client \
  ndscope \
  netbase \
  openssh-server \
  python-is-python3 \
  python3-awg \
  python3-foton \
  python3-pip \
  qemu-user \
  rpcbind \
  ssh \
  vim \
  x2goserver \
  && DEBIAN_FRONTEND=noninteractive apt-get install -qq --no-install-recommends \
  medm:amd64 \
  striptool:amd64 \
  && rm -rf /var/lib/apt/lists/* \
  && pip3 install --upgrade --break-system-packages pyqtgraph ndscope \
  && rm -rf /root/.cache/pip
# add medm wrappers (multiarch issues preclude installing them from packaging)
RUN wget -O medm_lho -nv https://git.ligo.org/cds/software/ligo-remote-epics-scripts/-/raw/master/medm_lho?inline=false \
  && wget -O medm_llo -nv https://git.ligo.org/cds/software/ligo-remote-epics-scripts/-/raw/master/medm_llo?inline=false \
  && chmod +x medm_lho medm_llo \
  && mv medm_lho medm_llo /usr/local/bin
# use ssh multiplexing
# add rtcds mount point
RUN mkdir -p /run/sshd && mkdir -p /root/.ssh && mkdir -p /opt/rtcds \
 && echo 'Host *' >/root/.ssh/config \
 && echo '  ControlMaster auto' >>/root/.ssh/config \
 && echo '  ControlPath ~/.ssh/control-%h-%p-%r' >>/root/.ssh/config \
 && echo '  ControlPersist 10m' >>/root/.ssh/config
# use ssh-agent from host only
# suppress lxpolkit error message by not allowing it to run
# light-locker dumps core on startup
RUN chmod -x /usr/bin/ssh-agent && chmod -x /usr/bin/lxpolkit && chmod -x /usr/bin/light-locker
# add init script
RUN echo '#!/bin/bash' >init.sh \
  && echo 'echo SSH_AUTH_SOCK="$SSH_AUTH_SOCK" >/root/.ssh/environment' >>init.sh \
  && echo 'echo RTSFORWARD_SSH_HOST="$RTSFORWARD_SSH_HOST" >>/root/.ssh/environment' >>init.sh \
  && echo 'echo RTSFORWARD_SSH_ARGS="$RTSFORWARD_SSH_ARGS" >>/root/.ssh/environment' >>init.sh \
  && echo '/usr/sbin/sshd -D -e -o StrictModes=no -o PermitUserEnvironment=yes -o X11UseLocalhost=no &' >>init.sh \
  && echo '/usr/sbin/x2gocleansessions -d &' >>init.sh \
  && echo 'wait -n' >>init.sh \
  && echo 'exit $?' >>init.sh \
  && chmod +x init.sh
# add sitemap alias
RUN echo "alias sitemap='if [ -z \"\$site\" ]; then echo \"error: unknown site (rtsforward not run or /tmp/rtsforward/env not sourced?)\"; else cd \"/opt/rtcds/\$site/\$ifo/medm\"; medm -x sitemap.adl & cd - >/dev/null; fi'" >/root/.bashrc
# add rtsforward scripts
COPY src/rtsforward /usr/local/bin
COPY src/register_awgtpman_rpc /usr/local/bin

EXPOSE 22
CMD /init.sh

