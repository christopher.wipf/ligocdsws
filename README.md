# ligocdsws
ligocdsws is a docker container that enables an ARM64-based Mac (M1 CPU or newer) to function as a remote CDS workstation. It provides LIGO control room software packaged for debian-arm64, so as to run at near-native speed. CDS network services (NDS, EPICS, awgtpman) are remotely accessed by forwarding their ports over an ssh connection inside the container.

## Usage
- Install colima and the docker CLI
  - For example, in MacPorts: `port install colima docker`
- Start the container: `./ligocdsws [user@cdsportal]`
  - `user@cdsportal` is an optional CDS login, passed to ssh in order to remotely mount the `/opt/rtcds` filesystem in the container (read only)
  - This command returns a shell running inside the container
  - Additional shells can be opened: `ssh -Y -p 22022 root@127.0.0.1` (on the host)
  - x2goclient can also be used to connect to the container if desired
- To remotely access CDS services: `rtsforward user@cdsportal; source /tmp/rtsforward/env` (in the container)
  - The corresponding `/opt/rtcds` filesystem must have been mounted in the previous step
- Run medm/ndscope/diaggui/etc as usual (in the container)
  - Save files under `shared` to make them available on the host (files saved anywhere else are lost when the container is stopped)
- When finished: `colima stop` (on the host)

## Known issues
rtsforward only detects and forwards EPICS services of advligorts models. Channels that are provided by slow controls or the DAQ system may be absent.

